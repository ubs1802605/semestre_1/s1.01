/**
* Jeu du Grundy
* @author SEVETTE Matiss
*/
import java.util.Arrays;

class Grundy1 {

    void principal() {
        lancerGrundy1();
    }





    /*********************Saisie du nombre d'allumettes************************/
    int saisieAllu () {
        //On entre le nombre d'allmuettes (>=5 sinon on ne peut pas jouer)
        int nb = SimpleInput.getInt("Sélectionnez le nombre " +
          "d'allumettes (>= 5): ");

        while (nb < 5) {
            System.out.println("Sélectionnez au moins 5 allumettes");
            nb = SimpleInput.getInt("Sélectionnez le nombre " +
              "d'allumettes : ");
        } 
        return nb;
    }
    
    void testSaisieAllu () {
        System.out.println ();
        System.out.println ("*** testSaisieAllu()");
        saisieAllu();
    }
    /********************FIN Saisie du nombre d'allumettes*********************/





    /************************Choix du type de partie***************************/
    int typePartie () {

        System.out.println("Choix du type de partie : ");
        System.out.println("- 0 = joueur contre joueur");
        System.out.println("- 1 = joueur contre ordinateur");

        int type = SimpleInput.getInt("Sélectionnez le type de " +
              "partie : ");

        while(type != 0 && type != 1) {
            type = SimpleInput.getInt("Sélectionnez le type de " +
              "partie : ");
        }

        return type;
    }
    
    void testTypePartie () {
        System.out.println ();
        System.out.println ("*** testTypePartie()");
        typePartie();
    }
    /************************FIN Choix du type de partie***********************/






    
    /************************Saisie du nom des joueurs*************************/
    String nomJoueur1 () {
        String nomJoueur1 = SimpleInput.getString("Sélectionnez le " +
          "nom du joueur 1 : ");
        return nomJoueur1;
    }

    String nomJoueur2 () {
        String nomJoueur2 = SimpleInput.getString("Sélectionnez le " +
          "nom du joueur 2 : ");
        return nomJoueur2;
    }
    
    void testNomJoueur1 () {
        System.out.println ();
        System.out.println ("*** testNomJoueur1()");
        nomJoueur1();
    }

    void testNomJoueur2 () {
        System.out.println ();
        System.out.println ("*** testNomJoueur2()");
        nomJoueur2();
    }
    /*********************FIN Saisie du nom des joueurs************************/






    /********************Saisie premier joueur*********************************/
    String premierJoueurChoix0 (String j1, String j2) {
        System.out.println("Choix du premier joueur : ");
        System.out.println("- 0 = " + j1);
        System.out.println("- 1 = " + j2);
        
        int choixPremierJoueur = SimpleInput.getInt("Sélectionnez " +
          "le premier joueur : ");
        while(choixPremierJoueur != 0 && choixPremierJoueur != 1) {
            choixPremierJoueur = SimpleInput.getInt("Sélectionnez " +
              "le premier joueur : ");
        }

        String premierJoueur;
        if (choixPremierJoueur == 0) {
            premierJoueur = j1;
        } else {
            premierJoueur = j2;
        }
        return premierJoueur;
    }

    String premierJoueurChoix1 (String j1, String j2) {
        System.out.println("Choix du premier joueur : ");
        System.out.println("- 0 = " + j1);
        System.out.println("- 1 = " + j2);

        int choixPremierJoueur = SimpleInput.getInt("Sélectionnez " +
        "le premier joueur : ");
        while(choixPremierJoueur != 0 && choixPremierJoueur != 1) {
            choixPremierJoueur = SimpleInput.getInt("Sélectionnez " +
              "le premier joueur : ");
        }

        String premierJoueur;
        if (choixPremierJoueur == 0) {
            premierJoueur = "**Ordinateur**";
        } else {
            premierJoueur = j2;
        }
        return premierJoueur;
    }
    
    void testPremierJoueurChoix0 () {
        System.out.println ();
        System.out.println ("*** testPremierJoueurChoix0()");

        String joueur1 = "Matiss";
        String joueur2 = "Sévette";
        String premierJoueur = premierJoueurChoix0(joueur1,joueur2);
        System.out.println("Le premier joueur est " + premierJoueur);
    }

    void testPremierJoueurChoix1 () {
        System.out.println ();
        System.out.println ("*** testPremierJoueurChoix1()");

        String joueur1 = "**Ordinateur**";
        String joueur2 = "Matiss";
        String premierJoueur = premierJoueurChoix1(joueur1,joueur2);
        System.out.println("Le premier joueur est " + premierJoueur);
    }
    /************************FIN Saisie premier joueur*************************/





    /************************Affichage de la ligne*****************************/
    void afficheLigne (int nbAllu) {
        int i = 0;
        while(i < nbAllu) {
            System.out.print("|  ");
            i++;
        }
        System.out.println();
    }

    void testAfficheLigne () {
        System.out.println();
        System.out.println("*** testAfficheLigne()");

        System.out.print("Ligne pour -2 : ");
        afficheLigne(-2);
        System.out.println ();

        System.out.print("Ligne pour 0 : ");
        afficheLigne(0);
        System.out.println ();

        System.out.print("Ligne pour 1 : ");
        afficheLigne(1);
        System.out.println ();

        System.out.print("Ligne pour 2 : ");
        afficheLigne(2);
        System.out.println ();

        System.out.print("Ligne pour 10 : ");
        afficheLigne(10);
        System.out.println ();
    }
    /************************FIN Affichage de la ligne*************************/





    /************************Numéro de la ligne********************************/
    int numeroLigne (int[] tab) {
        int numeroLigne = SimpleInput.getInt ("Sélectionnez une " +
          "ligne : ");

        boolean ligneAPlusDeTroisAllu = false;
        while (ligneAPlusDeTroisAllu == false) {
            while (numeroLigne > tab.length - 1 || numeroLigne < 0) {
                System.out.println("Ligne incorrecte");
                numeroLigne = SimpleInput.getInt ("Sélectionnez une " +
                  "ligne : ");
            }
            if (tab[numeroLigne] < 3) {
                System.out.println("Impossible de sélectionner la ligne " +
                  "car trop peu d'allumettes");
                numeroLigne = tab.length;
            } else {
                ligneAPlusDeTroisAllu = true;
            }
        }
        return numeroLigne;
    }

    void testNumeroLigne () {
        System.out.println();
        System.out.println("*** testNumeroLigne()");

        int[] tab1 = {10};
        int[] tab2 = {10,1,2,3};

        System.out.println(Arrays.toString(tab1));
        numeroLigne(tab1);
        System.out.println();
        System.out.println(Arrays.toString(tab2));
        numeroLigne(tab2);
    }
    /************************FIN Numéro de la ligne****************************/





    /***********Critères pour choisir le nombre d'allumettes à retirer*********/
    // La méthode rend true si le nb d'allmuettes à retirer est incorrect, 
    // et elle rend faux si le nb d'allmuettes à retirer est correct
    boolean criteresIncorrects(
          int[] tab, int numeroLigne, int nbAlluARetirer) {
        
        boolean criteresIncorrects = true;
        if (nbAlluARetirer == tab[numeroLigne]/2 
          || nbAlluARetirer > tab[numeroLigne] || nbAlluARetirer < 1 
            || nbAlluARetirer == tab[numeroLigne]) {
            criteresIncorrects = true;
        } else {
            criteresIncorrects = false;
        } 

        double nombreImpairDivisePar2 = tab[numeroLigne]/2;
        if (tab[numeroLigne] % 2 != 0 
          && nbAlluARetirer == (nombreImpairDivisePar2)) {
            criteresIncorrects = false;
        }
        return criteresIncorrects;
    }

    void testCriteresIncorrects() {
        System.out.println();
        System.out.println("*** testCriteresIncorrects()");

        int[] tab1 = {10,7};
        int numeroLigne1 = 0;
        int numeroLigne2 = 1;
        int nbAlluARetirer1 = 5;
        int nbAlluARetirer2 = 10;
        int nbAlluARetirer3 = 0;
        int nbAlluARetirer4 = -1;
        int nbAlluARetirer5 = 4;
        int nbAlluARetirer6 = 3;

        System.out.print("tab={10,7}, numeroLigne=0, et nbAlluARetirer=5 : ");
        System.out.println(
              criteresIncorrects(tab1, numeroLigne1, nbAlluARetirer1));

        System.out.print("tab={10,7}, numeroLigne=0, et nbAlluARetirer=10 : ");
        System.out.println(
              criteresIncorrects(tab1, numeroLigne1, nbAlluARetirer2));

        System.out.print("tab={10,7}, numeroLigne=0, et nbAlluARetirer=0 : ");
        System.out.println(
              criteresIncorrects(tab1, numeroLigne1, nbAlluARetirer3));

        System.out.print("tab={10,7}, numeroLigne=0, et nbAlluARetirer=-1 : ");
        System.out.println(
              criteresIncorrects(tab1, numeroLigne1, nbAlluARetirer4));

        System.out.print("tab={10,7}, numeroLigne=0, et nbAlluARetirer=4 : ");
        System.out.println(
              criteresIncorrects(tab1, numeroLigne1, nbAlluARetirer5));

        System.out.print("tab={10,7}, numeroLigne=1, et nbAlluARetirer=3 : ");
        System.out.println(
          criteresIncorrects(tab1, numeroLigne2, nbAlluARetirer6));
    }
    /********FIN Critères pour choisir le nombre d'allumettes à retirer********/





    /************************Nombre d'allumettes à retirer*********************/
    int nbAlluARetirer (int[] tab, int numeroLigne) {
        int nbAlluARetirer = SimpleInput.getInt ("Nombre d'allumettes " +
          "à retirer : ");
        boolean d = criteresIncorrects(tab, numeroLigne, nbAlluARetirer);

        //Si criteresIncorrects est à true, cela veut dire que 
        //nbAlluARetirer n'est pas valide, donc on redemande et on revérifie
        while (d == true) {
            System.out.println("Nombre d'allumettes à retirer incorrect");
            nbAlluARetirer = SimpleInput.getInt ("Nombre d'allumettes " +
              "à retirer : ");
            d = criteresIncorrects(tab, numeroLigne, nbAlluARetirer);
        }
        return nbAlluARetirer;
    }

    //testNbAlluARetirer inutile car c'est le même que testCriteresIncorrects

    /************************FIN Nombre d'allumettes à retirer*****************/





    /************************Séparer la ligne**********************************/
    int[] separerLigne (int[] tab, int numeroLigne, int nbAlluARetirer) {
        //Creation d'un tableau res d'une taille de 1 plus grande que tab avec
        //les mêmes valeurs que tab mais 0 en dernier indice
        int[] res = Arrays.copyOf(tab, tab.length + 1);

        /*  On déplace toutes les éléments du tableau res de 1 à droite à partir
            de du numéro de la ligne qui a été choisi
            EX : on retire 3 allumettes à la ligne 1 de tab :
            tab = {1,5,3,4} devient res = {1,3,2,3,4}
        */
        int i = res.length - 1;
        while (i > numeroLigne) {
            res[i] = res[i-1];
            res[i-1] = 0;
            i--;
        }

        res[numeroLigne] = nbAlluARetirer;
        res[numeroLigne + 1] = res[numeroLigne + 1] - nbAlluARetirer;
        System.out.println(Arrays.toString(res));

        return res;
    }

    void testSeparerLigne () {
        System.out.println ();
        System.out.println ("*** testSeparerLigne()");

        int[] tab1 = {1,5,3,2};
        int[] tab2 = {1,5,6,2};
        int[] tab3 = {1,5,3,7};
        int[] tab4 = {1,1,3,2};
        separerLigne(tab1, 1, 3);
        separerLigne(tab2, 2, 4);
        separerLigne(tab3, 3, 3);
        separerLigne(tab4, 2, 1);
    }
    /************************FIN Séparer la ligne******************************/





    /************************Afficher chaque ligne*****************************/
    void afficherChaqueLigne (int[] tab) {
        int i = 0; 
        while (i < tab.length) {
            System.out.print(i + " : ");
            afficheLigne(tab[i]);
            i++;
        }
    }

    void testAfficherChaqueLigne () {
        System.out.println ();
        System.out.println ("*** testAfficherChaqueLigne()");

        int[] tab1 = {1,5,3,2};
        int[] tab2 = {1,5,6,2};
        int[] tab3 = {1,5,3,7};
        int[] tab4 = {1,1,3,2};

        System.out.println("tab1 = {1,5,3,2}");
        afficherChaqueLigne(tab1);
        System.out.println("tab2 = {1,5,6,2}");
        afficherChaqueLigne(tab2);
        System.out.println("tab3 = {1,5,3,7}");
        afficherChaqueLigne(tab3);
        System.out.println("tab4 = {1,1,3,2}");
        afficherChaqueLigne(tab4);
    }
    /************************FIN Afficher chaque lignes************************/





    /************************Vérification si fin de partie*********************/
    boolean verificationFin (int[] tab) {
        //S'il ne reste que des 1 ou des 2 dans chaque ligne 
        //c'est la fin de la partie et la méthode renvoie true
        boolean fin = true;
        int i = 0;
        while (i < tab.length) {
            if (tab[i] > 2) {
                fin = false;
            }
            i++;
        }
        return fin;
    }

    void testVerificationFin () {
        System.out.println ();
        System.out.println ("*** testVerificationFin()");

        int[] tab1 = {1,5,3,2};
        int[] tab2 = {1,2,2,2};
        System.out.println("tab1 = {1,5,3,2}");
        System.out.println(verificationFin(tab1));
        System.out.println("tab2 = {1,2,2,2}");
        System.out.println(verificationFin(tab2));
    }
    /************************FIN Vérification si fin de partie*****************/


    /************************Tour du joueur************************************/
    void tourDuJoueur (String j1, String j2, String joueurPrecedent) {
        //La méthode affiche le joueur à qui c'est le tour de jouer
        if (joueurPrecedent == j1) { 
            System.out.println("Tour du joueur : " + j2);
        } else if (joueurPrecedent == j2) { 
            System.out.println("Tour du joueur : " + j1);

        }
    }
    /************************FIN Tour du joueur********************************/





    /************************Inverser joueur*********************************/
    String InverserTourDuJoueur (String j1, String j2, String joueurPrecedent) {
        if (joueurPrecedent == j1) { 
            joueurPrecedent = j2;
        } else if (joueurPrecedent == j2) { 
            joueurPrecedent = j1;
        }
        return joueurPrecedent;
    }
    /************************FIN Inverser joueur*******************************/





    /***********Lancer une partie de Grundy1 = joueur contre joueur************/
    void lancerGrundy1 () {
        //// joueur contre joueur = typePartie0

        int nbAllumettes = saisieAllu(); //Nb d'allumettes de la partie
        System.out.println("Choisir type 0 = joueur contre joueur");
        int typePartie = typePartie(); //Demande du type de la partie

        //Entrée nom des joueurs et sélection du joueur qui joue en 1er
        String j1;
        String j2;
        String premierJoueur;
        
        /*  Si typePartie==0 donc joueur contre joueur, on demande le nom des 
            deux joueurs et celui qui commencera.
            Si typePartie==1 donc ordinateur contre joueur, le joueur1 est 
            automatiquement nommé **Ordinateur** et on demande le nom du 
            joueur2 puis celui qui commencera 
        */
        if (typePartie == 0) {
            j1 = nomJoueur1();
            j2 = nomJoueur2();
            premierJoueur = premierJoueurChoix0(j1,j2);
        } else {
            j1 = "**Ordinateur**";
            j2 = nomJoueur2();
            premierJoueur = premierJoueurChoix1(j1,j2);
        }

        //Affiche la ligne 0 avec le nombre d'allumettes correspondant
        System.out.print("0 : ");
        afficheLigne(nbAllumettes);
        int[] tab = {nbAllumettes};
        System.out.println();

        //Le premier tour du jeu, le premier joueur joue sur la ligne 0
        System.out.println("Tour du joueur : " + premierJoueur);
        int l = 0; //Ligne 0
        int a = nbAlluARetirer(tab, l); //Entrée du nb d'allumettes à retirer
        int[] tab2 = separerLigne(tab, l, a); //Séparation des lignes
        afficherChaqueLigne(tab2); //Affichage des lignes
        System.out.println();

        //Vérification si fin de partie = obligé pour rentrer dans while
        boolean fin = verificationFin(tab2);
        
        //Pour plus de compréhensionn on remplace juste le nom de premierJoueur
        String joueurPrecedent = premierJoueur;

        //Tant que ce n'est pas la fin de la partie
        while (fin == false) {
            //On affiche le tour du joueur et on inverse le joueur qui à joué
            //avec le joueur précédent pour jouer chacun son tour
            tourDuJoueur(j1, j2, joueurPrecedent);
            joueurPrecedent = InverserTourDuJoueur(j1, j2, joueurPrecedent);

            l = numeroLigne(tab2); //Demande de la ligne
            a = nbAlluARetirer(tab2, l); //Demande nb d'allumettes à retirer
            tab2= separerLigne(tab2, l, a); //Séparation des lignes
            afficherChaqueLigne(tab2); //Affichage des lignes
            System.out.println(); System.out.println();

            fin = verificationFin(tab2); //Vérification fin de partie
        }

        //Si fin de partie, affichage du joueur gagnant
        System.out.println(joueurPrecedent + " a gagné la partie");
    }
    /***********FIN Lancer une partie de Grundy1 = joueur contre joueur********/
}
