/**
* Jeu du Grundy
* @author SEVETTE Matiss
*/

class Mémoire {


            //// joueur contre joueur = typePartie0

            int nbAllumettes = saisieAllu(); //Nb d'allumettes de la partie
            System.out.println("Choisir type 1 = Ordinateur contre joueur");
            int typePartie = typePartie(); //Demande du type de la partie
    
            //Entrée nom des joueurs et sélection du joueur qui joue en 1er
            String j1;
            String j2;
            String premierJoueur;
            
            /*  Si typePartie==0 donc joueur contre joueur, on demande le nom des 
                deux joueurs et celui qui commencera.
                Si typePartie==1 donc ordinateur contre joueur, le joueur1 est 
                automatiquement nommé **Ordinateur** et on demande le nom du 
                joueur2 puis celui qui commencera 
            */
            if (typePartie == 0) {
                j1 = nomJoueur1();
                j2 = nomJoueur2();
                premierJoueur = premierJoueurChoix0(j1,j2);
            } else {
                j1 = "**Ordinateur**";
                j2 = nomJoueur2();
                premierJoueur = premierJoueurChoix1(j1,j2);
            }
    
            //Affiche la ligne 0 avec le nombre d'allumettes correspondant
            System.out.print("0 : ");
            afficheLigne(nbAllumettes);
            int[] tab = {nbAllumettes};
            System.out.println();
    
            if (typePartie == 0) {
                //Le premier tour du jeu, le premier joueur joue sur la ligne 0
                System.out.println("Tour du joueur : " + premierJoueur);
                int l = 0; //Ligne 0
                int a = nbAlluARetirer(tab, l); //nb d'allumettes à retirer
                int[] tab2 = separerLigne(tab, l, a); //Séparation des lignes
                afficherChaqueLigne(tab2); //Affichage des lignes
                System.out.println();
    
                //Vérification si fin de partie = obligé pour rentrer dans while
                boolean fin = verificationFin(tab2);
                
                //Pour plus de compréhensionn on remplace le nom de premierJoueur
                String joueurPrecedent = premierJoueur;
    
                //Tant que ce n'est pas la fin de la partie
                while (fin == false) {
                    //On affiche le tour du joueur, on inverse le joueur qui à joué
                    //avec le joueur précédent pour jouer chacun son tour
                    tourDuJoueur(j1, j2, joueurPrecedent);
                    joueurPrecedent = InverserTourDuJoueur(j1, j2, joueurPrecedent);
    
                    l = numeroLigne(tab2); //Demande de la ligne
                    a = nbAlluARetirer(tab2, l); //Demande nb d'allumettes à retirer
                    tab2= separerLigne(tab2, l, a); //Séparation des lignes
                    afficherChaqueLigne(tab2); //Affichage des lignes
                    System.out.println(); System.out.println();
    
                    fin = verificationFin(tab2); //Vérification fin de partie
                }
    
                //Si fin de partie, affichage du joueur gagnant
                System.out.println(joueurPrecedent + " a gagné la partie");
            }
    
    
            if (typePartie == 1) {
                //Le premier tour du jeu, le premier joueur joue sur la ligne 0
                System.out.println("Tour du joueur : " + premierJoueur);
    
                boolean fin = verificationFin(tab); //Vérification fin de partie
                boolean jouerIntelligemement = jouerIntelligemement(tab);
                if (jouerIntelligemement == true && premierJoueur == j1) {
                    changementPartieIntelligente(tab, j1, j2, premierJoueur, fin);
                }
    
                int l = 0; //Ligne 0
                int a = -1;
                if (premierJoueur == j1) {
                    a = allumetteRandom(tab, l);
                } else {
                    a = nbAlluARetirer(tab, l); //nb d'allumettes à retirer
                }
    
                int[] tab2 = separerLigne(tab, l, a); //Séparation des lignes
                afficherChaqueLigne(tab2); //Affichage des lignes
                System.out.println();
    
                //Vérification si fin de partie = obligé pour rentrer dans while
                fin = verificationFin(tab2);
    
                //Pour plus de compréhensionn on remplace le nom de premierJoueur
                String joueurPrecedent = premierJoueur;
            
                //Tant que ce n'est pas la fin de la partie
                while (fin == false) {
                    //On affiche le tour du joueur, on inverse le joueur qui à joué
                    //avec le joueur précédent pour jouer chacun son tour
                    tourDuJoueur(j1, j2, joueurPrecedent);
                    joueurPrecedent = InverserTourDuJoueur(j1, j2, joueurPrecedent);
    
                    jouerIntelligemement = jouerIntelligemement(tab2);
    
                    if (jouerIntelligemement == true) {
                        tab2 = changementPartieIntelligente(tab2, j1, j2, premierJoueur, fin);
                        fin = verificationFin(tab2); //Vérification fin de partie
                    }
    
                    if (fin = false) {
                        if (joueurPrecedent == "**Ordinateur**") {
                            l = ligneRandom(tab2);
                            a = allumetteRandom(tab2, l);
                        } else {
                            l = numeroLigne(tab2); //Demande de la ligne
                            a = nbAlluARetirer(tab2, l); //nb d'allumettes à retirer
                        }
                        tab2= separerLigne(tab2, l, a); //Séparation des lignes
                        afficherChaqueLigne(tab2); //Affichage des lignes
                        System.out.println(); System.out.println();
    
                            
                        jouerIntelligemement = jouerIntelligemement(tab);
                        if (jouerIntelligemement == true) {
                            changementPartieIntelligente(tab, j1, j2, joueurPrecedent, fin);
                        }
                        
                        fin = verificationFin(tab2); //Vérification fin de partie
                    }
                }
    
                //Si fin de partie, affichage du joueur gagnant
                System.out.println(joueurPrecedent + " a gagné la partie");
    
        
    void principal() {
       /* 
        int nbAllumettes = saisieAllu();
        int typePartie = typePartie();

        String j1;
        String j2;
        String premierJoueur;
        if (typePartie == 0) {
            j1 = nomJoueur1();
            j2 = nomJoueur2();
            premierJoueur = premierJoueurChoix0(j1,j2);
        } else {
            j1 = "**Ordinateur**";
            j2 = nomJoueur2();
            premierJoueur = premierJoueurChoix1(j1,j2);
        }
        System.out.print("0 : ");
        
        afficheLigne(nbAllumettes);
        int[] tab = {nbAllumettes};
        System.out.println();

        System.out.println("Tour du joueur : " + premierJoueur);

        int l = 0;
        int a = nbAlluARetirer(tab, l);
        int[] tab2 = separerLigne(tab, l, a);
        afficherChaqueLigne(tab2);
        System.out.println();
        boolean fin = verificationFin(tab2);
        
        
        String joueurPrecedent = premierJoueur;
        while (fin == false) {
            tourDuJoueur(j1, j2, joueurPrecedent);
            joueurPrecedent = InverserTourDuJoueur(j1, j2, joueurPrecedent);

            l = numeroLigne(tab2);
            a = nbAlluARetirer(tab2, l);

            tab2= separerLigne(tab2, l, a);
            afficherChaqueLigne(tab2);
            System.out.println();

            fin = verificationFin(tab2);
            System.out.println();
        }

        System.out.println(joueurPrecedent + " a gagné la partie");
        */    }

    /************************Saisie du nombre d'allumettes*********************************/
    int saisieAllu () {
        
        int nb = SimpleInput.getInt("Sélectionnez le nombre d'allumettes : ");
        while (nb < 4) {
            System.out.println("Sélectionnez au moins 4 allumettes");
            nb = SimpleInput.getInt("Sélectionnez le nombre d'allumettes : ");
        } 
        System.out.println("Nombre d'allumettes : " + nb);

        return nb;
    }
    
    void testSaisieAllu () {
        System.out.println ();
        System.out.println ("*** testSaisieAllu()");
        saisieAllu();
    }
    /************************FIN Saisie du nombre d'allumettes*********************************/





    /************************Choix du type de partie********************************/
    int typePartie () {

        System.out.println("Choix du type de partie : ");
        System.out.println("- 0 = joueur contre joueur");
        System.out.println("- 1 = joueur contre ordinateur");

        int type = -1;
        while(type != 0 && type != 1) {
            type = SimpleInput.getInt("Sélectionnez le type de partie : ");
        }

        if (type == 0) {
            System.out.println("Vous avez choisi le mode 0 = joueur contre joueur !");
        } else {
            System.out.println("Vous avez choisi le mode 1 = joueur contre ordinateur !");
        }

        return type;
    }
    
    void testTypePartie () {
        System.out.println ();
        System.out.println ("*** testTypePartie()");
        typePartie();
    }
    /************************FIN Choix du type de partie**********************************/






    
    /************************Saisie du nom du premier joueur*********************************/
    String nomJoueur1 () {
        String nomJoueur1 = SimpleInput.getString("Sélectionnez le nom du joueur 1 : ");
        return nomJoueur1;
    }

    String nomJoueur2 () {
        String nomJoueur2 = SimpleInput.getString("Sélectionnez le nom du joueur 2 : ");
        return nomJoueur2;
    }
    
    void testNomJoueur1 () {
        System.out.println ();
        System.out.println ("*** testNomJoueur1()");
        nomJoueur1();
    }

    void testNomJoueur2 () {
        System.out.println ();
        System.out.println ("*** testNomJoueur2()");
        nomJoueur2();
    }
    /************************FIN Saisie du nom du premier joueur**********************************/






    /************************Saisie premier joueur*********************************/
    int premierJoueurChoix0 () {
        System.out.println("Choix du premier joueur : ");
        System.out.println("- 0 = joueur 1");
        System.out.println("- 1 = joueur 2");
        
        int premierJoueur = -1;
        while(premierJoueur != 0 && premierJoueur != 1) {
            premierJoueur = SimpleInput.getInt("Sélectionnez le premier joueur : ");
        }
        return premierJoueur;
    }

    int premierJoueurChoix1 () {
        System.out.println("Choix du premier joueur : ");
        System.out.println("- 0 = ordinateur");
        System.out.println("- 1 = joueur 2");

        int premierJoueur = -1;
        while(premierJoueur != 0 && premierJoueur != 1) {
            premierJoueur = SimpleInput.getInt("Sélectionnez le premier joueur : ");
        }

        return premierJoueur;
    }
    
    void testPremierJoueurChoix0 () {
        System.out.println ();
        System.out.println ("*** testPremierJoueurChoix0()");
        premierJoueurChoix0();
    }

    void testPremierJoueurChoix1 () {
        System.out.println ();
        System.out.println ("*** testPremierJoueurChoix1()");
        premierJoueurChoix1();
    }
    /************************FIN Saisie premier joueur**********************************/





    /************************Affichage de la ligne*********************************/
    void afficheLigne (int nbLigne, int nbAllu) {
        System.out.print(nbLigne + " : ");
        int i = 0;
        while(i < nbAllu) {
            System.out.print("|  ");
            i++;
        }
    }

    void testAfficheLigne () {
        System.out.println ();
        System.out.println ("*** testAfficheLigne()");

        /* 
        afficheLigne(0);
        System.out.println ();

        afficheLigne(1);
        System.out.println ();

        afficheLigne(2);
        System.out.println ();

        afficheLigne(10);
        System.out.println ();
        */
    }
    /************************FIN Affichage de la ligne*********************************/

    void initialisation () {
        int nbAllumettes = saisieAllu();
        int typePartie = typePartie();
        
        if (typePartie == 0) {
            String joueur1 = nomJoueur1();
            String joueur2 = nomJoueur2();
            System.out.println();
            System.out.println("Les joueurs sont donc : ");
            System.out.println("Joueur 1 = " + joueur1);
            System.out.println("Joueur 2 = " + joueur2);
            System.out.println();
        } 
        
        if (typePartie == 1) {
            String joueur1 = "**Ordinateur**";
            String joueur2 = nomJoueur2();
            System.out.println();

            System.out.println("Les joueurs sont donc : ");
            System.out.println("Joueur 1 = " + joueur1);
            System.out.println("Joueur 2 = " + joueur2);
            System.out.println();
        }

        afficheLigne(0, nbAllumettes);
        
    }



    
}
